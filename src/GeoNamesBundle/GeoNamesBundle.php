<?php

namespace ASI\GeoNames;

use ASI\GeoNames\Installer\Install;
use Pimcore\Extension\Bundle\AbstractPimcoreBundle;
use Pimcore\Extension\Bundle\Traits\PackageVersionTrait;

/**
 * Class GeoNamesBundle
 * @package ASI\GeoNames
 * @author  Sebastian Paulmichl <sebastian.paulmichl@asi.at>
 */
class GeoNamesBundle extends AbstractPimcoreBundle
{
    use PackageVersionTrait;

    const PACKAGE_NAME = 'asi/geonames';

    /**
     * {@inheritdoc}
     */
    public function getInstaller()
    {
        return $this->container->get(Install::class);
    }

    /**
     * {@inheritdoc}
     */
    protected function getComposerPackageName(): string
    {
        return self::PACKAGE_NAME;
    }
}
