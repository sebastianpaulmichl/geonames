<?php

namespace ASI\GeoNames\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 * @package ASI\GeoNames\DependencyInjection
 * @author  Sebastian Paulmichl <sebastian.paulmichl@asi.at>
 */
class Configuration implements ConfigurationInterface
{

    /**
     * Generates the configuration tree builder.
     *
     * @return \Symfony\Component\Config\Definition\Builder\TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('asi_geo_names');
        $rootNode
            ->children()
            ->scalarNode('apiKey')
            ->defaultNull()
            ->end()
            ->end();

        return $treeBuilder;
    }
}
