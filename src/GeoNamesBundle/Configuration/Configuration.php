<?php

namespace ASI\GeoNames\Configuration;

/**
 * Class Configuration
 * @package ASI\GeoNames\Configuration
 * @author  Sebastian Paulmichl <sebastian.paulmichl@asi.at>
 */
class Configuration
{
    const SYSTEM_CONFIG_DIR_PATH = PIMCORE_PRIVATE_VAR . '/bundles/GeoNamesBundle';
    const SYSTEM_CONFIG_FILE_PATH = PIMCORE_PRIVATE_VAR . '/bundles/GeoNamesBundle/config.yml';
    const SYSTEM_BACKUP_CONFIG_FILE_PATH = PIMCORE_PRIVATE_VAR . '/bundles/GeoNamesBundle/config_backup.yml';

    /**
     * @var array
     */
    private $config;

    /**
     * @param array $config
     */
    public function setConfig(array $config = []): void {
        $this->config = $config;
    }

    /**
     * @return array
     */
    public function getConfig(): array
    {
        return $this->config;
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    public function getConfigByKey(string $key)
    {
        return $this->config[$key];
    }
}
