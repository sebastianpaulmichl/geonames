<?php

namespace ASI\GeoNames\Installer;

use ASI\GeoNames\Configuration\Configuration;
use ASI\GeoNames\GeoNamesBundle;
use PackageVersions\Versions;
use Pimcore\Bundle\AdminBundle\Security\User\TokenStorageUserResolver;
use Pimcore\Extension\Bundle\Installer\AbstractInstaller;
use Pimcore\Extension\Bundle\Installer\Exception\InstallationException;
use Pimcore\Extension\Bundle\Installer\Exception\UpdateException;
use Pimcore\Model\DataObject\ClassDefinition;
use Pimcore\Model\DataObject\Folder;
use Pimcore\Model\Translation\Admin;
use Pimcore\Model\User;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Yaml\Yaml;

/**
 * Class Install
 * @package ASI\GeoNames\Installer
 * @author  Sebastian Paulmichl <sebastian.paulmichl@asi.at>
 */
class Install extends AbstractInstaller
{
    /**
     * @var TokenStorageUserResolver
     */
    private $resolver;

    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * @var string
     */
    private $installSourcesPath;

    /**
     * @var string
     */
    private $currentVersion;

    /**
     * @var array
     */
    private $classes = [
        'Country'
    ];

    /**
     * Install constructor.
     *
     * @param TokenStorageUserResolver   $resolver
     */
    public function __construct(
        TokenStorageUserResolver $resolver
    )
    {
        parent::__construct();

        $this->resolver = $resolver;
        $this->fileSystem = new Filesystem();

        $this->installSourcesPath = __DIR__ . '/../Resources/install';
        $this->currentVersion = Versions::getVersion(GeoNamesBundle::PACKAGE_NAME);
    }

    /**
     * Installs the bundle
     *
     * @throws InstallationException
     */
    public function install()
    {
        $this->installOrUpdateConfigFile();
        $this->installClasses();
        $this->installTranslations();
        $this->createFolders();
    }

    /**
     * Uninstalls the bundle
     *
     * @throws InstallationException
     */
    public function uninstall()
    {
        if($this->fileSystem->exists(Configuration::SYSTEM_CONFIG_FILE_PATH)) {
            $this->fileSystem->rename(
                Configuration::SYSTEM_CONFIG_FILE_PATH,
                Configuration::SYSTEM_BACKUP_CONFIG_FILE_PATH
            );
        }
    }

    /**
     * Determine if bundle is installed
     *
     * @return bool
     */
    public function isInstalled()
    {
        return $this->fileSystem->exists(Configuration::SYSTEM_CONFIG_FILE_PATH);
    }

    /**
     * Determine if bundle is ready to be installed. Can be used to check prerequisites
     *
     * @return bool
     */
    public function canBeInstalled()
    {
        return !$this->fileSystem->exists(Configuration::SYSTEM_CONFIG_FILE_PATH);
    }

    /**
     * Determine if bundle can be uninstalled
     *
     * @return bool
     */
    public function canBeUninstalled()
    {
        return $this->fileSystem->exists(Configuration::SYSTEM_CONFIG_FILE_PATH);
    }

    /**
     * Determines if admin interface should be reloaded after installation/uninstallation
     *
     * @return bool
     */
    public function needsReloadAfterInstall()
    {
        return true;
    }

    /**
     * Determine if bundle can be updated
     *
     * @return bool
     */
    public function canBeUpdated()
    {
        $needUpdate = false;
        if($this->fileSystem->exists(Configuration::SYSTEM_CONFIG_FILE_PATH)) {
            $config = Yaml::parse(file_get_contents(Configuration::SYSTEM_CONFIG_FILE_PATH));

            if($config['version'] !== $this->currentVersion) {
                $needUpdate = true;
            }
        }

        return $needUpdate;
    }

    /**
     * Updates the bundle
     *
     * @throws UpdateException
     */
    public function update()
    {
        $this->installOrUpdateConfigFile();
        $this->installClasses();
    }

    /**
     * install/update config file.
     */
    private function installOrUpdateConfigFile(): void
    {
        if(!$this->fileSystem->exists(Configuration::SYSTEM_CONFIG_DIR_PATH)) {
            $this->outputWriter->write('Creating configuration directory.');
            $this->fileSystem->mkdir(Configuration::SYSTEM_CONFIG_DIR_PATH);
        }

        $config = ['version' => $this->currentVersion];
        $yml = Yaml::dump($config);

        $this->outputWriter->write('Creating configuration file.');
        $this->fileSystem->dumpFile(Configuration::SYSTEM_CONFIG_FILE_PATH, $yml);
    }

    /**
     * Install model object classes.
     */
    private function installClasses(): void
    {
        $this->outputWriter->write('Installing classes.');
        foreach($this->getClasses() as $className => $path) {
            $class = new ClassDefinition();
            $id = $class->getDao()->getIdByName($className);

            if($id !== false) {
                continue;
            }

            $class->setName($className);

            $data = file_get_contents($path);

            ClassDefinition\Service::importClassDefinitionFromJson($class, $data, true);

            $this->outputWriter->write(
                sprintf(
                    'Installed Class "%s".',
                    $className
                )
            );
        }
    }

    /**
     * Retrieve the model classes.
     *
     * @return array
     */
    private function getClasses(): array
    {
        $classes = [];

        foreach($this->classes as $className) {
            $fileName = sprintf('class_%s_export.json', $className);
            $path = $this->installSourcesPath . '/classes/' . $fileName;
            if(!$this->fileSystem->exists($path)) {
                throw new InstallationException(
                    sprintf(
                        'Class export for class "%s" was expected in "%s" but file does not exist.',
                        $className, $path
                    )
                );
            }

            $classes[$className] = $path;
        }

        return $classes;
    }

    /**
     * Install the translations.
     * 
     * @throws InstallationException
     */
    private function installTranslations(): void
    {
        $this->outputWriter->write('Installing translations.');
        $csv = $this->installSourcesPath . '/translations/data.csv';

        try {
            Admin::importTranslationsFromFile($csv, true, Admin::getLanguages());
        } catch (\Exception $e) {
            // nope
        }
    }

    private function createFolders(): void
    {
        $this->outputWriter->write('Creating pimcore folders.');
        $root = Folder::getByPath('/geo_names');
        $countries = Folder::getByPath('/geo_names/countries');

        if(!$root instanceof Folder) {
            $this->outputWriter->write(
                sprintf(
                    'Creating "%s" folder.',
                    '/geo_names'
                )
            );
            $root = Folder::create([
                'o_parentId' => 1,
                'o_creationDate' => time(),
                'o_userOwner' => $this->getUserId(),
                'o_userModification' => $this->getUserId(),
                'o_key' => 'geo_names',
                'o_published' => true
            ]);
        }

        if(!$countries instanceof Folder) {
            $this->outputWriter->write(
                sprintf(
                    'Creating "%s" folder.',
                    '/geo_names/countries'
                )
            );
            Folder::create([
                'o_parentId' => $root->getId(),
                'o_creationDate' => time(),
                'o_userOwner' => $this->getUserId(),
                'o_userModification' => $this->getUserId(),
                'o_key' => 'countries',
                'o_published' => true
            ]);
        }
    }

    /**
     * Retrieve the user id.
     *
     * @return string
     */
    private function getUserId(): string
    {
        $userId = 0;
        $user = $this->resolver->getUser();

        if($user instanceof User) {
            $userId = $this->resolver->getUser()->geTId();
        }

        return $userId;
    }
}
